FROM python:3.9

RUN /usr/local/bin/python -m pip install --upgrade pip ; \
    pip install poetry==1.1.12 ; \
    poetry --version ; \
    poetry config virtualenvs.in-project true