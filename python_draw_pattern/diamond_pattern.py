from typing import List
from python_draw_pattern.pattern import Pattern


class DiamondPattern(Pattern):
    @staticmethod
    def generate_pattern_as_list(size) -> List[str]:
        """Generates a Diamond pattern as a List"""
        
        result = []
        for i in range(size):
            star_pattern = "*" * (i * 2 + 1)
            space_pattern = " " * (size - len(star_pattern) + i)
            line_result = space_pattern
            line_result += star_pattern
            line_result += space_pattern
            result.append(line_result)
        
        for i in range(1, size):
            space_pattern = " " * i
            star_pattern = "*" * (size * 2 - i * 2 - 1)

            line_result = space_pattern
            line_result += star_pattern
            line_result += space_pattern
            result.append(line_result)
        
        return result