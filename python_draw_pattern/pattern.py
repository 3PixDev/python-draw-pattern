from abc import abstractmethod
from enum import Enum
from typing import List

class PatternType(Enum):
    DIAMOND = 1
    PYRAMID = 2

class Pattern:
    @abstractmethod
    def generate_pattern_as_string(size) -> str:
        raise NotImplementedError("This function is part of an Abstract Base Class and must be implemented by a subclass.")

    @abstractmethod
    def generate_pattern_as_list(size) -> List[str]:
        pass