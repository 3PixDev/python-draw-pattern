"""
Module in charge of generating a Pyramid Pattern
"""

from typing import List
from python_draw_pattern.pattern import Pattern


class PyramidPattern(Pattern):
    """Child class for generating a Pattern.
    This class will generate a Pyramid pattern.
    0 ___*___
    1 __***__
    2 _*****_
    3 *******
    """

    @staticmethod
    def generate_pattern_as_string(size: int) -> str:
        '''Generate a Pyramid Pattern as a String'''
        return "\n".join(PyramidPattern.generate_pattern_as_list(size))

    @staticmethod
    def generate_pattern_as_list(size: int) -> List[str]:
        """Generate a Pyramid Pattern as a List[str]"""
        result = []
        for i in range(size):
            space_pattern = " " * int((size * 2 - 2) / 2 - i)
            star_pattern = "*" * (i * 2 + 1)
            line_result = space_pattern
            line_result += star_pattern
            line_result += space_pattern
            result.append(line_result)

        return result

