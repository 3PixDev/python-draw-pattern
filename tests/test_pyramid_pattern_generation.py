import pytest

from python_draw_pattern.pyramid_pattern import PyramidPattern

@pytest.mark.parametrize("n, expected", [
    (1, "*"),
    (2, " * \n***"),
    (3, "  *  \n *** \n*****"),
    (4, "   *   \n  ***  \n ***** \n*******"),
])
def test_pyramid_pattern_is_generated_correctly_as_string(n, expected):
    result = PyramidPattern.generate_pattern_as_string(n)

    assert result == expected, f"Expected:\n{expected}\n Got:\n{result}"


@pytest.mark.parametrize("n, expected", [
    (1, ["*"]),
    (2, [" * ", "***"]),
    (3, ["  *  ", " *** ", "*****"]),
    (4, ["   *   ", "  ***  ", " ***** ", "*******"]),
])
def test_pyramid_pattern_is_generated_correctly_as_list(n, expected):
    result = PyramidPattern.generate_pattern_as_list(n)

    assert result == expected, f"Expected:\n{expected}\n Got:\n{result}"