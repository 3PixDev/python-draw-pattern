
from python_draw_pattern.pattern import PatternType


def test_diamond_pattern_is_available():
    assert "DIAMOND" in PatternType.__dict__

def test_pyramid_pattern_is_available():
    assert "PYRAMID" in PatternType.__dict__

def test_potato_pattern_is_not_available():
    assert "POTATO" not in PatternType.__dict__